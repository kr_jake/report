package com.jake.report;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Broadcast sender/receiver
 */
public class Main02Activity extends Activity {

    //filter
    private final String IntentFilter = "com.jake.report.broadcastreceiver.filter";

    //receiver
    private MyBroadCastReceiver mReceiver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_02);

        //text
        findViewById(R.id.send_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    //send to receiver
                    //text data and sendting time
                    String sendData = ((TextView)findViewById(R.id.send_data)).getText().toString();
                    Calendar cal = Calendar.getInstance();
                    String dateToString , timeToString ;
                    dateToString = String.format("%04d-%02d-%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
                    timeToString = String.format("%02d:%02d:%02d", cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

                    Intent intent = new Intent(IntentFilter);
                    intent.putExtra("msg",sendData);
                    intent.putExtra("time",dateToString + " " + timeToString);
                    sendBroadcast(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver();
    }

    public class MyBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String msg = intent.getStringExtra("msg");
            String time = intent.getStringExtra("time");
            ((TextView)findViewById(R.id.receive_data)).setText(time + "\n" + msg);
        }
    }

    /**
     * Create an instance of BroadcastReceiver
     * and regist to on this activity
     */
    private void registerReceiver(){
        if(mReceiver != null) return;
        final android.content.IntentFilter filter = new IntentFilter();
        filter.addAction(IntentFilter);
        this.mReceiver = new MyBroadCastReceiver();
        this.registerReceiver(this.mReceiver, filter);
    }

    /**
     * unregist receiver
     */
    private void unregisterReceiver() {
        if(mReceiver != null){
            this.unregisterReceiver(mReceiver);
            mReceiver = null;
        }
    }

}
