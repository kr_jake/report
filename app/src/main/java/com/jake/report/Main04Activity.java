package com.jake.report;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Main04Activity extends Activity {

    private File tempFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_04);

        //request permission
        tedPermission();

        //taking photo
        findViewById(R.id.take_photo_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    takePhoto();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //get from gallery
        findViewById(R.id.get_photo_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    takeFromAlbum();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void tedPermission() {

        //handling permission
        PermissionListener permissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {}

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) { }
        };

        //request
        TedPermission.with(this)
                .setPermissionListener(permissionListener)
                .setRationaleMessage("resuest permission")
                .setDeniedMessage("please request permission")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check();

    }

    /**
     * take from album
     */
    private void takeFromAlbum() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, 1000);
    }

    /**
     * take photo
     * request to system with a file
     * finally, save a photo file on the system
     */
    private void takePhoto() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            //create a file for saving
            tempFile = createImageFile();
        } catch (IOException e) {
            Toast.makeText(this, "error : " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            return;
        }
        if (tempFile != null) {
            //Uri photoUri = Uri.fromFile(tempFile);
            //make a uri to fileprovider(this app)
            Uri photoUri = FileProvider.getUriForFile(Main04Activity.this, "com.jake.report.fileprovider", tempFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(intent, 1001);
        }
    }

    /**
     * create a file
     * @return
     * @throws IOException
     */
    private File createImageFile() throws IOException {

        // file name ( jake_time_ )
        String timeStamp = new SimpleDateFormat("HHmmss").format(new Date());
        String imageFileName = "jake_" + timeStamp + "_";

        // directory name ( jake )
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/jake/");
        if (!storageDir.exists()) storageDir.mkdirs();

        // create a emtpy file
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        return image;
    }

    /**
     * draw a image from tempFile
     */
    private void setImage() {
        if(tempFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(tempFile.getAbsolutePath());
            ImageView myImage = (ImageView) findViewById(R.id.image_view);
            myImage.setImageBitmap(myBitmap);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != RESULT_OK) {
            Toast.makeText(this, "canceled.", Toast.LENGTH_SHORT).show();
            if(tempFile != null) {
                if (tempFile.exists()) {
                    if (tempFile.delete()) {
                        tempFile = null;
                    }
                }
            }
            return;
        }

        if (requestCode == 1000) {
            //from gallery
            Uri photoUri = data.getData();
            Cursor cursor = null;
            try {
                String[] proj = { MediaStore.Images.Media.DATA };
                assert photoUri != null;
                cursor = getContentResolver().query(photoUri, proj, null, null, null);
                assert cursor != null;
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                tempFile = new File(cursor.getString(column_index));
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }

            setImage();
        } else if (requestCode == 1001) {
            //from taking photo
            Toast.makeText(this, "completed.", Toast.LENGTH_SHORT).show();
            setImage();
            //request media scanning
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + tempFile.getAbsolutePath()));
            sendBroadcast(mediaScanIntent);
        }
    }

}
