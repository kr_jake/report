package com.jake.report;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Request/response thru the internet
 *  - text data transfer such as login
 *  - binary data transfer
 */
public class Main01Activity extends Activity {

    //Gallery request code
    public final int INTENT_REQUEST_KEY_FROM_GALLERY = 1000;

    //image path
    private String imagePath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_01);

        //text data transfer such as login
        findViewById(R.id.send_text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.send_text).setVisibility(View.VISIBLE);
                findViewById(R.id.send_binary).setVisibility(View.GONE);
                findViewById(R.id.text_data).setVisibility(View.VISIBLE);
                findViewById(R.id.send_text_data).setVisibility(View.VISIBLE);
                findViewById(R.id.select_image).setVisibility(View.GONE);
            }
        });

        //binary data transfer
        findViewById(R.id.send_binary).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.send_text).setVisibility(View.GONE);
                findViewById(R.id.send_binary).setVisibility(View.VISIBLE);
                findViewById(R.id.text_data).setVisibility(View.GONE);
                findViewById(R.id.send_text_data).setVisibility(View.GONE);
                findViewById(R.id.select_image).setVisibility(View.VISIBLE);
            }
        });

        //send
        findViewById(R.id.send_text_data).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    new TextDataUploadTask().execute(((EditText) findViewById(R.id.text_data)).getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //Photo select
        findViewById(R.id.select_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFromGallery();
            }
        });

    }

    @Override
    public void onBackPressed() {
        //check state
        if (findViewById(R.id.send_text).getVisibility() == View.GONE || findViewById(R.id.send_binary).getVisibility() == View.GONE) {
            //return back to prev step
            findViewById(R.id.send_text).setVisibility(View.VISIBLE);
            findViewById(R.id.send_binary).setVisibility(View.VISIBLE);
            findViewById(R.id.text_data).setVisibility(View.GONE);
            findViewById(R.id.send_text_data).setVisibility(View.GONE);
            findViewById(R.id.select_image).setVisibility(View.GONE);
        } else{
            super.onBackPressed();
        }
    }

    /**
     * get image from system gallery
     */
    private void getFromGallery() {
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_PICK);
        // Chooser of file system options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "please select a image");
        startActivityForResult(chooserIntent, INTENT_REQUEST_KEY_FROM_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //result from gallery
        if (resultCode == RESULT_OK && requestCode == INTENT_REQUEST_KEY_FROM_GALLERY) {
            if (data == null) {
                Toast.makeText(getApplicationContext(), "Unable to Pickup Image", Toast.LENGTH_LONG).show();
                return;
            }
            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = cursor.getString(columnIndex);
                new BinaryDataUploadTask().execute();
            } else {
                Toast.makeText(getApplicationContext(), "Unable to Load Image", Toast.LENGTH_LONG).show();
            }

        }
    }

    /**
     * Text upload
     *
     */
    private  class TextDataUploadTask extends AsyncTask<String, Integer, Object> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Main01Activity.this);
            progressDialog.setMessage("text uploading start....");
            progressDialog.show();
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                OkHttpClient client = new OkHttpClient();
                //String url = "http://533e2a63.ngrok.io/report/requestTextData";
                String url = "http://15.164.94.3:8080/Report/requestTextData";
                RequestBody formBody = new FormBody.Builder()
                        .add("textData", (String)params[0])
                        .build();
                Request request = new Request.Builder()
                        .url(url)
                        .post(formBody)
                        .build();
                Response response = client.newCall(request).execute();
                Log.i("test","request : " + request.toString());
                Log.i("test","Response : " + response.toString());
                Log.i("test","Response body : " + response.body().toString());
                return response.code()+"";
            } catch (Exception e) {
                Log.i("TAG", "Error : " + e.getLocalizedMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null)
                    progressDialog.dismiss();
                if (result != null && result.equals("200")) {
                    Toast.makeText(getApplicationContext(), "success text request", Toast.LENGTH_LONG).show();
                    finish();
                } else
                    Toast.makeText(getApplicationContext(), "fail text request", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "fail text request - " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * image upload
     */
    private  class BinaryDataUploadTask extends AsyncTask<String, Integer, Object> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Main01Activity.this);
            progressDialog.setMessage("binary uploading start....");
            progressDialog.show();
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                FileInputStream fis = new FileInputStream(imagePath);
                URL url=new URL("http://15.164.94.3:8080/Report/uploadBinary");
                //URL url = new URL("http://533e2a63.ngrok.io/report/uploadBinary");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setUseCaches(false);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=files");
                DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
                dos.writeBytes("--files\r\n");
                dos.writeBytes("Content-Disposition: form-data; name=\"file1\"; filename=\"" + imagePath + "\"" + "\r\n");
                dos.writeBytes("\r\n");
                int bytes = fis.available();
                int maxBufferSize = 1024;
                int bufferSize = Math.min(bytes, maxBufferSize);
                byte[] buffer = new byte[bufferSize];
                int read = fis.read(buffer, 0, bufferSize);
                while (read > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytes = fis.available();
                    bufferSize = Math.min(bytes, maxBufferSize);
                    read = fis.read(buffer, 0, bufferSize);
                }
                dos.writeBytes("\r\n");
                dos.writeBytes("--files--\r\n");
                fis.close();
                dos.flush();
                dos.close();
                int ch;
                InputStream is = conn.getInputStream();
                StringBuffer sb = new StringBuffer();
                while ((ch = is.read()) != -1) {
                    sb.append((char) ch);
                }
                String str = sb.toString().trim();

                is.close();
                conn.disconnect();
                return str;
            } catch (Exception e) {
                e.printStackTrace();

                return e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null)
                    progressDialog.dismiss();
                if (result != null) {
                    Toast.makeText(Main01Activity.this, result.toString(), Toast.LENGTH_SHORT).show();
                    finish();
                } else
                    Toast.makeText(getApplicationContext(), "fail upload file", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "fail upload file - " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

}
