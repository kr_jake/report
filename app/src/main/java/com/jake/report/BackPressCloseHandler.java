package com.jake.report;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by www on 2017-01-03.
 */

public class BackPressCloseHandler {

    private long backKeyPressedTime = 0;
    private Toast toast;
    private Activity activity;

    public BackPressCloseHandler(Activity context) {
        this.activity = context;
    }

    public void onBackPressed() {
//        if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
//            backKeyPressedTime = System.currentTimeMillis();
//            showGuide();
//            return;
//        }
//        if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
            activity.finishAffinity();
            Intent i =  new Intent(activity, MainActivity.class);
            activity.startActivity(i);
//            toast.cancel();
//        }
    }
    public void showGuide() {
        toast = Toast.makeText(activity, "f", Toast.LENGTH_SHORT); toast.show();
    }
}
